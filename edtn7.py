import random
import requests
import re
import datetime
import waitress
import logging

from logging.handlers import RotatingFileHandler
from icalendar import Calendar
from flask import Flask

REFRESH_AFTER = 2

cache = {}
colors = {}

logger = logging.getLogger('waitress')
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')

file_handler = RotatingFileHandler('access.log', 'a', 1000000, 1)
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.INFO)

logger.addHandler(file_handler)


def get_color(name):
    if name not in colors.keys():
        colors[name] = '#{:02x}{:02x}{:02x}'.format(
            random.randint(0, 255), random.randint(0, 255),
            random.randint(0, 255))
    return colors[name]


def get_cals(resources):
    now = datetime.datetime.now()

    if resources in cache.keys():
        if cache[resources][0] > now - datetime.timedelta(hours=REFRESH_AFTER):
            return cache[resources][1]

    if now.month > 8:
        base_year = now.year
    else:
        base_year = now.year - 1

    url = 'https://edt.inp-toulouse.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=' + resources + '&projectId=20&calType=ical&firstDate=' + str(
        base_year) + '-09-01&lastDate=' + str(base_year + 1) + '-08-31'

    cal = Calendar.from_ical(requests.get(url).text)

    for component in cal.walk():
        if component.name == "VEVENT":
            c = re.sub(r'(.*)(C(?:M|ours)|TP|(?:C?TD)) (.*)', r'\1 \3 (\2)',
                       component['summary'])
            component['color'] = get_color(c.split(' ')[0])
            c = re.sub(
                r'(((([A-Z]+\d)|(\d+[A-Z]))[A-Z\d-]*|([A-Z] ?et ?[A-Z])| |-|&)+ - )+',
                r'',
                c,
                flags=re.I).strip()
            c = re.sub(r'(.*)(Exam(?:en)?)(.*)', r'EXAM - \1 \3', c)
            component['summary'] = c[0].upper() + c[1:]

    cache[resources] = (now, cal.to_ical())
    return cal.to_ical()


app = Flask(__name__)


@app.route("/<resources>")
def cal(resources):
    # Integer check
    for r in resources.split(','):
        int(r)
    return get_cals(resources)


waitress.serve(app, host='0.0.0.0', port=5000)
